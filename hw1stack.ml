type 'a stack = 
   | Content of 'a list;;  (* stack built on top of an list *)

let start f = f (Content []);;
let push x stk f = f (match stk with
   | Content list -> x::list);;
let pop stk f = f (match stk with
   | Content list -> (match list with
      | [] -> []
      | x::xs -> xs));; 
let add stk f = f (match stk with
   | Content list -> (match list with
      | [] -> [] 
      | [_] -> list
      | x::y::xs -> x + y :: xs));;
let mult stk f = f (match stk with
   | Content list -> (match list with
      | [] -> []
      | [_] -> list 
      | x::y::xs -> x * y :: xs));;
let clone stk f = f (match stk with
   | Content list -> (match list with
      | [] -> []
      | x::xs -> x :: x :: xs));;
let kpop stk f = f (match stk with
   | Content list -> (match list with
      | [] -> []
      | k::ks -> let rec loop stack i = match stack with
                    | [] -> []
                    | x::xs -> begin if i = 0 then xs else loop xs (i-1) end
                    in loop ks k));;
let halt stk = match stk with 
   | Content list -> list;;   (* returns. *)