let rec pow x n = if n = 0 then 1 else if n = 1 then x else x * pow x (n-1);;
let rec float_pow x n = if n = 0 then 1.0 else if n = 1 then x else x *. float_pow x (n-1);;

let compress list = 
   let rec is_in n slst = 
      match slst with
      | [] -> false
      | x::xs ->
           begin
               if x = n then true
               else is_in n xs
           end
   in
   let rec loop l r =
      match r with
      | [] -> l
      | x::xs ->
           begin
               if is_in x l then loop l xs
               else loop (x::l) r
           end
   in
   List.rev (loop [] list);;

let remove_if list func =
   let rec loop l r =
       match r with
       | [] -> l
       | x::xs ->
            begin
               if func x then loop l xs
               else loop (x::l) xs
            end
    in
    List.rev (loop [] list);;

let rec slice list i j =
    if i > ((List.length list) - 1) then []
    else if i >= j then []
    else if j > ((List.length list) - 1) then (List.nth list i) :: (slice list (i+1) ((List.length list) - 1))
    else (List.nth list i) :: (slice list (i+1) (j));;

(* let equivs func list = *)
(* skips *)

let goldbachpair n = 
    let is_prime a =
       let rec no_d m =
          m * m > a || (a mod m != 0 && no_d (m+1))
    in
       n>=2 && no_d 2
    in
    let rec find_p x = 
        if is_prime x && is_prime (n-x) then (x, n-x)
        else find_p (x+1)
    in find_p 2;;

let equiv_on f g lst = 
    if (List.map f lst) = (List.map g lst) then true
    else false;;

let pairwisefilter cmp lst =
    let rec loop l r =
          match r with
          | [] -> l
          | x::[] -> x::l
          | x::y::xs -> loop ((cmp x y)::l) xs
    in
    List.rev (loop [] lst);;


let polynomial list k =
    let rec pow base exp =
          if exp = 0 then 1
          else if exp = 1 then base
          else base * pow base (exp-1) 
    in
    let tuple_p (a,b) c = 
          a * pow c b
    in
    let rec sum_p lst n =
          match lst with
          | [] -> 0
          | x::xs -> tuple_p x n + sum_p xs n
    in
    sum_p list k;;

let rec powerset set =
    match set with
    | [] -> [[]]
    | x::xs -> let s = powerset xs in 
         s @ List.map (fun ss -> x::ss) s;; (* mapping every single part to a longer part *)
