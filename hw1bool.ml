type bool_expr = 
    | Lit of string
    | Not of bool_expr
    | And of bool_expr * bool_expr
    | Or of bool_expr * bool_expr;;

let truth_table x y exp = 
    let rec bool_eval va vb expr = 
          match va, vb with
          | true, true -> (match expr with
             | Lit s -> true
             | Not e -> not (bool_eval va vb e)
             | And (e1,e2) -> ((bool_eval va vb e1) && (bool_eval va vb e2))
             | Or (e1,e2) -> ((bool_eval va vb e1) || (bool_eval va vb e2)))
          | true, false -> (match expr with
             | Lit s -> begin if s = x then true else false end
             | Not e -> not (bool_eval va vb e)
             | And (e1,e2) -> ((bool_eval va vb e1) && (bool_eval va vb e2))
             | Or (e1,e2) -> ((bool_eval va vb e1) || (bool_eval va vb e2)))
          | false, true -> (match expr with
             | Lit s -> begin if s = x then false else true end
             | Not e -> not (bool_eval va vb e)
             | And (e1,e2) -> ((bool_eval va vb e1) && (bool_eval va vb e2))
             | Or (e1,e2) -> ((bool_eval va vb e1) || (bool_eval va vb e2)))
          | false, false -> (match expr with
             | Lit s -> false
             | Not e -> not (bool_eval va vb e)
             | And (e1,e2) -> ((bool_eval va vb e1) && (bool_eval va vb e2))
             | Or (e1,e2) -> ((bool_eval va vb e1) || (bool_eval va vb e2)))
    in
    [(true, true, (bool_eval true true exp)); (true, false, (bool_eval true false exp)); (false, true, (bool_eval false true exp)); (false, false, (bool_eval false false exp))];;